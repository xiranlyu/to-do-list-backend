package com.tw.todoitems.db;

import com.tw.todoitems.model.Item;
import com.tw.todoitems.model.ItemStatus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ItemRepo {
    private Connector connector;

    public ItemRepo() {
        this.connector = new Connector();
    }

    public Item addItem(Item item) {
        try{
            String query = "INSERT INTO toDo (`text`, `status`) VALUES(?, ?);";
            String queryId = "SELECT `id` FROM toDo WHERE `text` = \"" + item.getText() + "\";";
            Connection newConnection = connector.createConnect();
            PreparedStatement preparedStatement = newConnection.prepareStatement(query);
            preparedStatement.setString(1, item.getText());
            preparedStatement.setString(2, item.getStatus().name());
            preparedStatement.execute();
            Statement newStatement = newConnection.createStatement();
            ResultSet resultSet = newStatement.executeQuery(queryId);
            resultSet.next();
            item.setId(resultSet.getInt("id"));
            newConnection.close();
            return item;
        } catch (SQLException ignored) {
        }
        return null;
    }

    public List<Item> findItems() {
        List<Item> itemList = new ArrayList<>();
        try {
            Connection newConnection = connector.createConnect();
            String query = "SELECT * FROM toDo";
            Statement newStatement = newConnection.createStatement();
            ResultSet result = newStatement.executeQuery(query);
            while (result.next()) {
                itemList.add(new Item(result.getInt("id"), result.getString("text"),
                        ItemStatus.valueOf(result.getString("status"))));
            }
            newConnection.close();
            return itemList;
        } catch (SQLException ignored) {
        }
        return null;
    }

    public boolean updateItem(Item item) {
        try {
            String query = "UPDATE toDo SET `text` = ?, `status` = ? WHERE `id` = ?;";
            Connection newConnection = connector.createConnect();
            PreparedStatement preparedStatement = newConnection.prepareStatement(query);
            preparedStatement.setString(1, item.getText());
            preparedStatement.setString(2, item.getStatus().name());
            preparedStatement.setInt(3, item.getId());
            preparedStatement.execute();
            newConnection.close();
            return true;
        } catch (SQLException ignored) {
        }
        return false;
    }

    public boolean deleteItem(int id) {
        try {
            Connection newConnection = connector.createConnect();
            String query = "DELETE FROM toDo WHERE `id` = " + id;
            PreparedStatement newStatement = newConnection.prepareStatement(query);
            newStatement.execute();
            newConnection.close();
            return true;
        } catch (SQLException ignored) {
        }
        return false;
    }

    public boolean deleteAll() {
        try {
            Connection newConnection = connector.createConnect();
            String query = "DELETE FROM toDo";
            PreparedStatement newStatement = newConnection.prepareStatement(query);
            newStatement.execute();
            newConnection.close();
            return true;
        } catch (SQLException ignored) {
        }
        return false;
    }
}
